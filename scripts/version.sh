#!/bin/bash
cd "${MESON_SOURCE_ROOT:=$(dirname $0)/..}"

version="$(git describe --abbrev=0)"
if [ "$version" = "" ]; then
    version="0.0.0"
fi

patch="$(git rev-list --count ${version}..HEAD)"
echo "${version}-${patch}"
