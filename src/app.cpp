/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "app.hpp"
#include "iface/os.hpp"
#include "library.hpp"
#include "logging.hpp"
#include "tui/bar.hpp"
#include "tui/board.hpp"
#include "tui/screen.hpp"
#include <iostream>
#include <map>
#include <stdexcept>
using namespace taskpp;
using std::placeholders::_1;

static Logger logger(__FILENAME__);

bool App::handle_quit(char)
{
    return false;
}

bool App::handle_resize(char)
{
    auto &ncurses = ncurses();

    log_debug("App::handle_resize: {}x{}", ncurses.columns(), ncurses.rows());

    // Erase the entire screen.
    ncurses.werase(term.stdscr()->pointer());

    // Resize bounds.
    screen.window(HEADER_BAR)->resize(0, 0, ncurses.columns(), BAR_HEIGHT);
    screen.window(FOOTER_BAR)
        ->resize(0, ncurses.rows() - BAR_HEIGHT, ncurses.columns(),
                 BAR_HEIGHT);

    term.stdscr()->resize();
    return true;
}

int App::init_keybinds(void)
{
    // Setup keybindings.
    static int q = config.get<char>("keybind.quit");
    map.emplace(q, std::bind(&App::handle_quit, this, _1));
    map.emplace(KEY_RESIZE, std::bind(&App::handle_resize, this, _1));
    return 0;
}

App::~App(void)
{
    if (_ofs) {
        logger.error_stream(std::cout);
        logger.output_stream(std::cout);
    }
}

int App::init(int argc, char **argv)
{
    auto parse_error = config.parse_args(argc, argv);
    if (parse_error)
        return parse_error;

    _ofs = os().open(config.get<std::string>("logfile"),
                     std::ios::out | std::ios::app);
    logger.output_stream(*_ofs);
    logger.error_stream(*_ofs);

    if (config.has("help")) {
        return config.help(std::cout, RETURN_HELP);
    } else if (config.has("config-help")) {
        return config.config_help(std::cout, RETURN_HELP);
    } else if (config.has("version")) {
        return rc_raw_log(RETURN_HELP, "{}", VERSION);
    } else if (config.get<bool>("verbose")) {
        logger.set_level(DEBUG);
        log_debug("Debug logging enabled.");
    }

    return init_keybinds();
}

bool App::handle_input(int ch)
{
    if (auto kv = map.find(ch); kv != map.end())
        return kv->second(ch);
    return true;
}

int App::run(void)
{
    term.start();
    screen.init(term.stdscr(), term.root());

    // Build our interface on the root handle
    auto root = screen.window(ROOT);

    auto header = std::make_shared<Bar>(root, 0);
    header->content(fmt::format("{} - v{}", PROJECT_NAME, VERSION)).draw();
    screen.emplace(ROOT, HEADER_BAR, header);

    screen.emplace(ROOT, BOARD, std::make_shared<Board>(root, 1));

    auto footer = std::make_shared<Bar>(root, term.rows() - 1);
    std::string url(PROJECT_URL);
    footer->content(PROJECT_URL).draw();
    screen.emplace(ROOT, FOOTER_BAR, footer);
    root->refresh_all();

    auto &ncurses = ncurses();
    while (handle_input(ncurses.keypress())) {
        // Loop until handle_input returns false.
    }

    return 0;
}
