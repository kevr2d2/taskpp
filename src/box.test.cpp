/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "box.hpp"
#include <gtest/gtest.h>
using namespace taskpp;

TEST(Box, works)
{
    // Construct with dimensions.
    Box box(1, 2, 3, 4);
    ASSERT_EQ(box.x(), 1);
    ASSERT_EQ(box.y(), 2);
    ASSERT_EQ(box.width(), 3);
    ASSERT_EQ(box.height(), 4);

    // Copy construct
    Box box2(box);
    ASSERT_EQ(box2.x(), 1);
    ASSERT_EQ(box2.y(), 2);
    ASSERT_EQ(box2.width(), 3);
    ASSERT_EQ(box2.height(), 4);

    // Move construct
    Box box3(std::move(box2));
    ASSERT_EQ(box.x(), box3.x());
    ASSERT_EQ(box.y(), box3.y());
    ASSERT_EQ(box.width(), box3.width());
    ASSERT_EQ(box.height(), box3.height());

    // Resize the box.
    box3.resize(6, 7, 8, 9);

    // Move assign
    box2 = std::move(box3);
    ASSERT_EQ(box2.x(), 6);
    ASSERT_EQ(box2.y(), 7);
    ASSERT_EQ(box2.width(), 8);
    ASSERT_EQ(box2.height(), 9);
}
