/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "box.hpp"
using namespace taskpp;

Box::Box(int x, int y, int width, int height)
    : _x(x)
    , _y(y)
    , _width(width)
    , _height(height)
{
}

Box::Box(const Box &other)
    : _x(other._x)
    , _y(other._y)
    , _width(other._width)
    , _height(other._height)
{
}

Box &Box::operator=(const Box &other)
{
    resize(other._x, other._y, other._width, other._height);
    return *this;
}

void Box::resize(int x, int y, int width, int height)
{
    _x = x;
    _y = y;
    _width = width;
    _height = height;
}

int Box::x(void) const
{
    return _x;
}

int Box::y(void) const
{
    return _y;
}

int Box::width(void) const
{
    return _width;
}

int Box::height(void) const
{
    return _height;
}
