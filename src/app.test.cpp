/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "app.hpp"
#include "library.hpp"
#include "mocks/ncurses.hpp"
#include <gtest/gtest.h>
using namespace taskpp;
using testing::_;
using testing::AtLeast;
using testing::AtMost;
using testing::NiceMock;
using testing::Return;

class AppTest : public testing::Test
{
protected:
    NiceMock<MockNcurses> mock_ncurses;
    App app;

    int init_rc;

public:
    void SetUp(void)
    {
        set_library("ncurses", mock_ncurses);

        // Mock terminal setup path
        WINDOW *root = reinterpret_cast<WINDOW *>(1);
        WINDOW *win = reinterpret_cast<WINDOW *>(2);
        EXPECT_CALL(mock_ncurses, initscr())
            .Times(AtLeast(0))
            .WillRepeatedly(Return(root));
        EXPECT_CALL(mock_ncurses, subwin(_, _, _, _, _))
            .Times(AtLeast(0))
            .WillRepeatedly(Return(win));
        EXPECT_CALL(mock_ncurses, endwin())
            .Times(AtLeast(0))
            .WillRepeatedly(Return(OK));
        EXPECT_CALL(mock_ncurses, columns())
            .Times(AtLeast(0))
            .WillRepeatedly(Return(COLS));
        EXPECT_CALL(mock_ncurses, rows())
            .Times(AtLeast(0))
            .WillRepeatedly(Return(LINES));
    }

    void TearDown(void)
    {
        restore_library();
    }
};

template <typename... Args>
static int app_init(App &app, Args &&...args)
{
    int argc = sizeof...(args);
    std::vector<const char *> vec({ args... });
    return app.init(argc, const_cast<char **>(vec.data()));
}

TEST_F(AppTest, runs)
{
    ASSERT_EQ(app_init(app, "taskpp"), 0);

    EXPECT_CALL(mock_ncurses, keypress())
        .Times(AtMost(2))
        .WillOnce(Return('a'))
        .WillOnce(Return('q'));
    ASSERT_EQ(app.run(), 0);
}

TEST_F(AppTest, resize)
{
    ASSERT_EQ(app_init(app, "taskpp"), 0);

    EXPECT_CALL(mock_ncurses, keypress())
        .Times(AtMost(2))
        .WillOnce(Return(KEY_RESIZE))
        .WillOnce(Return('q'));
    ASSERT_EQ(app.run(), 0);
}
