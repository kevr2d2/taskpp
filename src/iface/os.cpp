/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "os.hpp"
#include <cstdlib>
#include <fstream>
#include <memory>
using namespace taskpp;

const char *OS::getenv(const char *name) const
{
    return ::getenv(name);
}

std::shared_ptr<std::ostream> OS::open(const std::string &fn,
                                       std::ios_base::openmode modes) const
{
    auto *ofs = new std::ofstream(fn, modes);
    return std::shared_ptr<std::ostream>(ofs);
}
