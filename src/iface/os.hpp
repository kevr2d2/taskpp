/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef IFACE_OS_HPP
#define IFACE_OS_HPP

#include <iostream>
#include <memory>

namespace taskpp
{

class OSInterface
{
public:
    virtual ~OSInterface(void) = default;
    virtual const char *getenv(const char *name) const = 0;

    virtual std::shared_ptr<std::ostream>
    open(const std::string &fn, std::ios_base::openmode modes) const = 0;
};

class OS : public OSInterface
{
public:
    ~OS(void) = default;
    const char *getenv(const char *name) const final override;

    std::shared_ptr<std::ostream>
    open(const std::string &fn,
         std::ios_base::openmode modes) const final override;
};

}; // namespace taskpp

#endif /* IFACE_OS_HPP */
