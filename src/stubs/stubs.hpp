/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef STUBS_STUBS_HPP
#define STUBS_STUBS_HPP

// Declare a stub's function
#define STUB_DECL(R, name, ...) R name(__VA_ARGS__);

// Define a stub's function
#define STUB(R, name, default_value, ...)                                     \
    R name(__VA_ARGS__)                                                       \
    {                                                                         \
        return default_value;                                                 \
    }

#endif /* STUBS_STUBS_HPP */
