/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "ncurses.hpp"

// LCOV_EXCL_START
extern "C" {

// Stub forward declaration of WINDOW.
struct _win_st {
    char _;
};

STUB(WINDOW *, initscr, nullptr, void)
STUB(WINDOW *, subwin, nullptr, WINDOW *, int, int, int, int)
STUB(int, refresh, OK, void)
STUB(int, wrefresh, OK, WINDOW *)
STUB(int, delwin, OK, WINDOW *)
STUB(int, endwin, OK, void)
STUB(int, noecho, OK, void)
STUB(int, curs_set, OK, int)
STUB(int, wborder, OK, WINDOW *, chtype, chtype, chtype, chtype, chtype,
     chtype, chtype, chtype)
STUB(int, wmove, OK, WINDOW *, int, int)
STUB(int, mvwprintw, OK, WINDOW *, int, int, const char *, ...)
STUB(bool, has_colors, true, void)
STUB(int, start_color, OK, void)
STUB(int, alloc_pair, 0, int, int)
STUB(int, wattron, OK, WINDOW *, int)  // T?
STUB(int, wattroff, OK, WINDOW *, int) // T?
STUB(int, use_default_colors, OK, void)
STUB(int, wbkgd, OK, WINDOW *, chtype)
STUB(int, getch, 0, void)
STUB(int, wgetch, 0, WINDOW *)
STUB(int, mvwin, OK, WINDOW *, int, int)
STUB(int, wresize, OK, WINDOW *, int, int)
STUB(int, wclear, OK, WINDOW *)
STUB(int, werase, OK, WINDOW *)
STUB(int, keypad, OK, WINDOW *, bool)
STUB(int, raw, OK, void)
};
// LCOV_EXCL_STOP
