/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef STUBS_NCURSES_HPP
#define STUBS_NCURSES_HPP

#include "stubs.hpp"

#define FALSE 0
#define TRUE 1

#define OK FALSE
#define ERR TRUE

#define chtype char

#define COLS 80
#define LINES 40

#define COLOR_BLACK 0
#define COLOR_RED 1
#define COLOR_GREEN 2
#define COLOR_YELLOW 3
#define COLOR_BLUE 4
#define COLOR_MAGENTA 5
#define COLOR_CYAN 6
#define COLOR_WHITE 7

#define COLOR_PAIR(x) 0

#define KEY_RESIZE 0

extern "C" {
// forward decl
struct _win_st;
typedef struct _win_st WINDOW;

STUB_DECL(WINDOW *, initscr, void)
STUB_DECL(WINDOW *, subwin, WINDOW *, int, int, int, int)
STUB_DECL(int, refresh, void)
STUB_DECL(int, wrefresh, WINDOW *)
STUB_DECL(int, delwin, WINDOW *)
STUB_DECL(int, endwin, void)
STUB_DECL(int, noecho, void)
STUB_DECL(int, curs_set, int)
STUB_DECL(int, wborder, WINDOW *, chtype, chtype, chtype, chtype, chtype,
          chtype, chtype, chtype)
STUB_DECL(int, wmove, WINDOW *, int, int)
STUB_DECL(int, mvwprintw, WINDOW *, int, int, const char *, ...)
STUB_DECL(bool, has_colors, void)
STUB_DECL(int, start_color, void)
STUB_DECL(int, alloc_pair, int, int)
STUB_DECL(int, wattron, WINDOW *, int)  // T?
STUB_DECL(int, wattroff, WINDOW *, int) // T?
STUB_DECL(int, use_default_colors, void)
STUB_DECL(int, wbkgd, WINDOW *, chtype)
STUB_DECL(int, getch, void)
STUB_DECL(int, wgetch, WINDOW *)
STUB_DECL(int, mvwin, WINDOW *, int, int)
STUB_DECL(int, wresize, WINDOW *, int, int)
STUB_DECL(int, wclear, WINDOW *)
STUB_DECL(int, werase, WINDOW *)
STUB_DECL(int, keypad, WINDOW *, bool)
STUB_DECL(int, raw, void)
};

#endif /* STUBS_NCURSES_HPP */
