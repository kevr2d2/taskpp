/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "../iface/os.hpp"
#include <gmock/gmock.h>
#include <iostream>
#include <memory>

namespace taskpp
{

class MockOS : public OSInterface
{
public:
    MOCK_METHOD(const char *, getenv, (const char *), (const, override));
    MOCK_METHOD(std::shared_ptr<std::ostream>, open,
                (const std::string &, std::ios_base::openmode),
                (const, override));
};

}; // namespace taskpp
