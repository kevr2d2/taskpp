/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef APP_HPP
#define APP_HPP

#include "config.hpp"
#include "tui/screen.hpp"
#include "tui/terminal.hpp"
#include <functional>
#include <iostream>
#include <map>

namespace taskpp
{

class App
{
private:
    //! Application configuration object
    Config config;

    std::shared_ptr<std::ostream> _ofs;

    //! TUI management objects
    Terminal term;
    Screen screen;

    // Callbacks
    std::map<int, std::function<bool(char)>> map;

private:
    //! keybind.quit handler
    bool handle_quit(char ch);

    //! Resize handler
    bool handle_resize(char ch);

    //! Initialize the application keybinds
    int init_keybinds(void);

public:
    //! Destruct an App
    ~App(void);

    //! Initialize the application
    int init(int argc, char **argv);

    //! Handle a single keypress
    bool handle_input(int ch);

    //! Run the application
    int run(void);
};

}; // namespace taskpp

#endif /* APP_HPP */
