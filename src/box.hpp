/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef BOX_HPP
#define BOX_HPP

namespace taskpp
{

class Box
{
private:
    int _x = 0;
    int _y = 0;
    int _width = 0;
    int _height = 0;

public:
    Box(void) = default;
    Box(int x, int y, int width, int height);
    Box(const Box &other);
    virtual ~Box(void) = default;

    Box &operator=(const Box &other);

    virtual void resize(int x, int y, int width, int height);

    int x(void) const;
    int y(void) const;
    int width(void) const;
    int height(void) const;
};

}; // namespace taskpp

#endif /* BOX_HPP */
