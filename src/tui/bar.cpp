/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "bar.hpp"
#include "../config.hpp"
#include "color.hpp"
using namespace taskpp;

Bar::Bar(std::shared_ptr<Window> parent, int y)
{
    set_parent(*parent);
    Box::operator=(Box(0, y, ncurses().columns(), BAR_HEIGHT));
    init(x(), this->y(), width(), height()).set_color(get_color(COLOR_BAR));
}

Bar &Bar::content(std::string value)
{
    _content = std::move(value);
    return *this;
}

void Bar::resize(void)
{
    ncurses().werase(ptr);
    Window::resize();
    set_color(get_color(COLOR_BAR));
    draw();
    refresh();
}

void Bar::draw(void)
{
    auto &ncurses = ncurses();
    auto delta =
        ncurses.columns() - static_cast<int>(_content.size()) - PADDING * 2;
    std::string padding;
    std::string output;
    if (delta > 1) {
        padding = std::string(delta, ' ');
        output = std::string(padding + _content + " ");
    } else {
        output = std::string(ncurses.columns(), ' ');
    }
    ncurses.mvwprintw(*this, 0, 0, output);
}
