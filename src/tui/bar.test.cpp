/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#include "bar.hpp"
#include "../library.hpp"
#include "../mocks/ncurses.hpp"
#include <gtest/gtest.h>
using namespace taskpp;
using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

class BarTest : public testing::Test
{
protected:
    NiceMock<MockNcurses> mock_ncurses;

public:
    void SetUp(void)
    {
        set_library("ncurses", mock_ncurses);
    }

    void TearDown(void)
    {
        restore_library();
    }
};

TEST_F(BarTest, draw_undersized)
{
    const int WIDTH = 2;

    auto parent = std::make_shared<Window>(reinterpret_cast<WINDOW *>(1));
    auto child_ptr = reinterpret_cast<WINDOW *>(2);
    EXPECT_CALL(mock_ncurses, subwin(_, _, _, _, _))
        .WillRepeatedly(Return(child_ptr));

    EXPECT_CALL(mock_ncurses, columns())
        .Times(AtLeast(1))
        .WillRepeatedly(Return(WIDTH));

    Bar bar(parent, 0);

    // Variables updated by the mock invocation.
    WINDOW *win_;
    int y_, x_;
    std::string str_;

    auto mock_mvwprintw = [&](auto *win, auto y, auto x,
                              const auto &str) -> int {
        win_ = win;
        y_ = y;
        x_ = x;
        str_ = str;
        return OK;
    };
    EXPECT_CALL(mock_ncurses, mvwprintw(_, _, _, _))
        .Times(AtLeast(1))
        .WillOnce(Invoke(mock_mvwprintw));

    bar.draw();

    ASSERT_EQ(win_, child_ptr);
    ASSERT_EQ(y_, 0);
    ASSERT_EQ(x_, 0);
    ASSERT_EQ(str_, std::string(WIDTH, ' '));
}
