/**
 * Copyright (C) 2022 Kevin Morris
 * Complete GPLv2 text can be found in LICENSE.
 **/
#ifndef TUI_BAR_HPP
#define TUI_BAR_HPP

#include "window.hpp"

namespace taskpp
{

static const int BAR_HEIGHT = 1;

class Bar : public Window
{
private:
    //! Content to be drawn on the bar
    std::string _content;

public:
    //! Construct and initialize a Bar at y
    Bar(std::shared_ptr<Window> parent, int y);

    //! Destruct a Bar
    ~Bar(void) = default;

    //! Set the Bar's content
    Bar &content(std::string value);

    /**
     * @brief Resize the Bar
     *
     * Previous to this function being call, Bar's dimensions
     * should be updated via resize(int x, int y, int w, int h).
     **/
    void resize(void) final override;

    /**
     * @brief Draw the Bar
     *
     * This function will attempt to display the Bar's content set
     * previously using Bar::content(std::string). If the content
     * overflows the Bar's width, we fallback to displaying an empty
     * bar.
     **/
    void draw(void) final override;
};

}; // namespace taskpp

#endif /* TUI_BAR_HPP */
